resource "google_compute_address" "default" {
  project = local.subnetwork_project
  region  = var.region

  # This name includes the project ID because it must be unique
  # because the address is added to the host project
  name         = "${var.name}-${var.project}-internal"
  address_type = "INTERNAL"
  subnetwork   = var.subnetwork_self_link
}

resource "google_compute_disk" "data_disk" {
  project = var.project

  name = "${local.name_full}-data"
  zone = var.zone
  size = var.data_disk_size

  type     = var.data_disk_type
  snapshot = var.data_disk_snapshot_override == "" ? one(data.google_compute_snapshot.data_disk_snapshot[*].id) : var.data_disk_snapshot_override

  labels = var.labels

  lifecycle {
    ignore_changes = [snapshot]
  }

  timeouts {
    create = var.data_disk_create_timeout
  }
}

resource "google_compute_disk" "log_disk" {
  project = var.project

  name = "${local.name_full}-log"
  zone = var.zone
  size = var.log_disk_size
  type = var.log_disk_type

  labels = var.labels
}

resource "google_compute_disk_resource_policy_attachment" "default" {
  project = var.project

  name = google_compute_resource_policy.data_disk_snapshot_policy.name
  disk = google_compute_disk.data_disk.name
  zone = google_compute_disk.data_disk.zone
  lifecycle {
    # This lifecycle rule is necessary because you cannot modify policies when
    # they are in use
    replace_triggered_by = [
      google_compute_resource_policy.data_disk_snapshot_policy.id
    ]
  }
}

resource "google_compute_disk" "os_disk_from_snapshot" {
  count = var.os_disk_snapshot_search_string != "" ? 1 : 0

  project = var.project

  zone     = var.zone
  snapshot = data.google_compute_snapshot.os_disk_snapshot[0].self_link
  name     = local.name_full
  labels   = var.labels
  type     = var.os_disk_type
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_instance" "default" {
  project = var.project

  allow_stopping_for_update = var.allow_stopping_for_update
  machine_type              = var.machine_type
  deletion_protection       = var.deletion_protection
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = format("%v.c.%v.internal", local.name_full, var.project)
    },
  )
  name = local.name_full
  zone = var.zone

  service_account {
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  boot_disk {
    auto_delete = true
    source      = var.os_disk_snapshot_search_string == "" ? null : google_compute_disk.os_disk_from_snapshot[0].self_link
    dynamic "initialize_params" {
      for_each = var.os_disk_snapshot_search_string == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }
  attached_disk {
    source = google_compute_disk.data_disk.self_link
  }

  attached_disk {
    source      = google_compute_disk.log_disk.self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = var.subnetwork_self_link
  }

  labels = var.labels

  tags = [
    var.labels.type,
    var.environment,
  ]
  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}
