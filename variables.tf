variable "allow_stopping_for_update" {
  type        = bool
  description = "Whether Terraform is allowed to stop the instance to update its properties"
  default     = false
}

variable "bootstrap_data_disk" {
  type        = string
  description = "Bootstrap script should detect, format, and mount data disk"
  default     = "true"
}

variable "bootstrap_script" {
  type        = string
  description = "user-provided bootstrap script to override the bootstrap module"
}

variable "chef_provision" {
  type = object({
    server_url            = string
    version               = string
    bootstrap_gcp_project = optional(string)
    bootstrap_bucket      = string
    bootstrap_keyring     = string
    bootstrap_key         = string
  })
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "data_disk_create_timeout" {
  type        = string
  description = "Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot."
  default     = "30m"
}

variable "data_disk_size" {
  type        = number
  description = "The size of the data disk"
}

variable "data_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 2 weeks"
  default     = 14
}

variable "data_disk_type" {
  type        = string
  description = "The type of the data disk"
  default     = "pd-balanced"
}

variable "deletion_protection" {
  type    = bool
  default = false
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "format_data_disk" {
  type        = string
  description = "Force formatting of the persistent disk."
  default     = "false"
}

variable "initial_boot_force_reboot" {
  type        = string
  description = "Force an additional reboot after the initial boot, this ensures the log disk is cleanly mounted after a rebuild."
  default     = "true"
}

variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources. You must at least include a type label"
  validation {
    condition     = lookup(var.labels, "type", "") != ""
    error_message = "This module requires a type label"
  }
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "name" {
  type        = string
  description = "The instance name"
}

variable "zone" {
  type        = string
  description = "The instance availability zone"
}

variable "data_disk_snapshot_search_string" {
  type        = string
  description = "If set, will be used to lookup the most recent snapshot using the provided string as a filter, and use it for the data disk"
  default     = ""
}

variable "data_disk_snapshot_override" {
  type        = string
  description = "If set, will be used to pass in the selfLink of the desired snapshot, to use for the data disk"
  default     = ""
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 40
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-ssd"
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "region" {
  type        = string
  description = "The target region"
  default     = null
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "subnetwork_self_link" {
  type        = string
  description = "Self-link of the subnetwork for the instance."
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "user-provided teardown script to override the bootstrap module"
}

variable "os_disk_snapshot_search_string" {
  type        = string
  description = "Set to the name of an instance, in order to use an existing snapshot of it's boot disk for a new node."
  default     = ""
}

variable "enable_os_disk_snapshots" {
  type        = bool
  description = "Enable creation of OS disk snapshot resource and attachment policies"
  default     = false
}

variable "hours_between_os_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly os disk snapshots, 0 to disable"
  default     = 0
}

variable "enable_os_snapshot_guest_scripts" {
  type    = bool
  default = true
}

variable "os_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 3."
  default     = 3
}

# Provides a mechanism to launch instances based on snapshots from another project.
# Primarily used for testing disaster recovery scenarios without touching the original
# project's Terraform state.
variable "snapshot_project" {
  type        = string
  description = "Project from which snapshots should be used from during provisioning. This should generally be unset and left default"
  default     = ""
}
