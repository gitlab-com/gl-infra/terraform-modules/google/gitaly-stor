locals {
  tier               = "stor"
  name_full          = "${var.name}-${local.tier}-${var.environment}"
  subnetwork_project = data.google_compute_subnetwork.subnetwork.project
  instance_metadata = {
    "CHEF_URL"                     = var.chef_provision.server_url
    "CHEF_VERSION"                 = var.chef_provision.version
    "CHEF_ENVIRONMENT"             = var.environment
    "CHEF_RUN_LIST"                = var.chef_run_list
    "CHEF_DNS_ZONE_NAME"           = var.dns_zone_name
    "CHEF_PROJECT"                 = coalesce(var.chef_provision.bootstrap_gcp_project, var.project)
    "CHEF_BOOTSTRAP_BUCKET"        = var.chef_provision.bootstrap_bucket
    "CHEF_BOOTSTRAP_KEYRING"       = var.chef_provision.bootstrap_keyring
    "CHEF_BOOTSTRAP_KEY"           = var.chef_provision.bootstrap_key
    "GL_BOOTSTRAP_DATA_DISK"       = var.bootstrap_data_disk
    "GL_PERSISTENT_DISK_PATH"      = "/var/opt/gitlab"
    "GL_FORMAT_DATA_DISK"          = var.format_data_disk
    "GL_INITIAL_BOOT_FORCE_REBOOT" = var.initial_boot_force_reboot
    "block-project-ssh-keys"       = "TRUE"
    "enable-oslogin"               = "FALSE"
    "shutdown-script"              = var.teardown_script
    "startup-script"               = var.bootstrap_script
  }

  // all the defaults plus cloudkms to access kms
  service_account_scopes = [
    "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/pubsub",
    "https://www.googleapis.com/auth/service.management.readonly",
    "https://www.googleapis.com/auth/servicecontrol",
    "https://www.googleapis.com/auth/trace.append",
    "https://www.googleapis.com/auth/cloudkms",
    "https://www.googleapis.com/auth/compute.readonly",
  ]
}
