output "zone" {
  value = google_compute_instance.default.zone
}

output "name" {
  value = google_compute_instance.default.name
}

output "project" {
  value = google_compute_instance.default.project
}

output "ipaddress" {
  value = google_compute_instance.default.network_interface[0].network_ip
}
