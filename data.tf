data "google_compute_snapshot" "data_disk_snapshot" {
  count = var.data_disk_snapshot_search_string == "" ? 0 : 1

  project     = coalesce(var.snapshot_project, var.project)
  filter      = "(sourceDisk eq .*/${var.data_disk_snapshot_search_string}-.*)(status eq 'READY')"
  most_recent = true
}

data "google_compute_snapshot" "os_disk_snapshot" {
  count = var.os_disk_snapshot_search_string == "" ? 0 : 1

  project     = coalesce(var.snapshot_project, var.project)
  filter      = "(sourceDisk eq .*/${var.os_disk_snapshot_search_string})(status eq 'READY')"
  most_recent = true
}

data "google_compute_subnetwork" "subnetwork" {
  # We use this data resource to get the project that corresponds
  # to the subnetwork_self_link passed into the module.
  self_link = var.subnetwork_self_link
}
