resource "google_compute_resource_policy" "data_disk_snapshot_policy" {
  project = var.project
  region  = var.region

  name = "${local.name_full}-snapshot-policy"
  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = 1
        start_time     = "00:00"
      }
    }

    retention_policy {
      max_retention_days    = var.data_disk_snapshot_max_retention_days
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}


resource "google_compute_resource_policy" "os_disk_snapshot_policy" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project
  region  = var.region

  name = format(
    "%v-os-snapshot-policy",
    var.name
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.hours_between_os_disk_snapshots
        start_time     = "00:00"
      }
    }

    snapshot_properties {
      guest_flush = var.enable_os_snapshot_guest_scripts
      labels      = var.labels
    }

    retention_policy {
      max_retention_days    = var.os_disk_snapshot_max_retention_days
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }
  }
}

resource "google_compute_disk_resource_policy_attachment" "instance_with_attached_disk_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project

  name = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk = google_compute_instance.default.name
  zone = google_compute_instance.default.zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}
