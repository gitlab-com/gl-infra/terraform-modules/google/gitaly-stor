# GitLab.com Gitaly Storage Terraform Module

## What is this?

Provisions resources to run a production grade [gitaly](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/README.md) service:

1. A GCE instance.
1. Hourly snapshots for recovery purpose.
1. Multiple disks.
1. A static internal address.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_address.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_disk.data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.os_disk_from_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_attached_disk_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_instance.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_resource_policy.data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_resource_policy.os_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_snapshot.data_disk_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_snapshot) | data source |
| [google_compute_snapshot.os_disk_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_snapshot) | data source |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_subnetwork) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Whether Terraform is allowed to stop the instance to update its properties | `bool` | `false` | no |
| <a name="input_bootstrap_data_disk"></a> [bootstrap\_data\_disk](#input\_bootstrap\_data\_disk) | Bootstrap script should detect, format, and mount data disk | `string` | `"true"` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | user-provided bootstrap script to override the bootstrap module | `string` | n/a | yes |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | <pre>object({<br>    server_url            = string<br>    version               = string<br>    bootstrap_gcp_project = optional(string)<br>    bootstrap_bucket      = string<br>    bootstrap_keyring     = string<br>    bootstrap_key         = string<br>  })</pre> | n/a | yes |
| <a name="input_chef_run_list"></a> [chef\_run\_list](#input\_chef\_run\_list) | run\_list for the node in chef | `string` | n/a | yes |
| <a name="input_data_disk_create_timeout"></a> [data\_disk\_create\_timeout](#input\_data\_disk\_create\_timeout) | Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot. | `string` | `"30m"` | no |
| <a name="input_data_disk_size"></a> [data\_disk\_size](#input\_data\_disk\_size) | The size of the data disk | `number` | n/a | yes |
| <a name="input_data_disk_snapshot_max_retention_days"></a> [data\_disk\_snapshot\_max\_retention\_days](#input\_data\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 2 weeks | `number` | `14` | no |
| <a name="input_data_disk_snapshot_override"></a> [data\_disk\_snapshot\_override](#input\_data\_disk\_snapshot\_override) | If set, will be used to pass in the selfLink of the desired snapshot, to use for the data disk | `string` | `""` | no |
| <a name="input_data_disk_snapshot_search_string"></a> [data\_disk\_snapshot\_search\_string](#input\_data\_disk\_snapshot\_search\_string) | If set, will be used to lookup the most recent snapshot using the provided string as a filter, and use it for the data disk | `string` | `""` | no |
| <a name="input_data_disk_type"></a> [data\_disk\_type](#input\_data\_disk\_type) | The type of the data disk | `string` | `"pd-balanced"` | no |
| <a name="input_deletion_protection"></a> [deletion\_protection](#input\_deletion\_protection) | n/a | `bool` | `false` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_enable_os_disk_snapshots"></a> [enable\_os\_disk\_snapshots](#input\_enable\_os\_disk\_snapshots) | Enable creation of OS disk snapshot resource and attachment policies | `bool` | `false` | no |
| <a name="input_enable_os_snapshot_guest_scripts"></a> [enable\_os\_snapshot\_guest\_scripts](#input\_enable\_os\_snapshot\_guest\_scripts) | n/a | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_format_data_disk"></a> [format\_data\_disk](#input\_format\_data\_disk) | Force formatting of the persistent disk. | `string` | `"false"` | no |
| <a name="input_hours_between_os_disk_snapshots"></a> [hours\_between\_os\_disk\_snapshots](#input\_hours\_between\_os\_disk\_snapshots) | Setting this will enable hourly os disk snapshots, 0 to disable | `number` | `0` | no |
| <a name="input_initial_boot_force_reboot"></a> [initial\_boot\_force\_reboot](#input\_initial\_boot\_force\_reboot) | Force an additional reboot after the initial boot, this ensures the log disk is cleanly mounted after a rebuild. | `string` | `"true"` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. You must at least include a type label | `map(string)` | n/a | yes |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The machine size | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The instance name | `string` | n/a | yes |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-2004-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `40` | no |
| <a name="input_os_disk_snapshot_max_retention_days"></a> [os\_disk\_snapshot\_max\_retention\_days](#input\_os\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 3. | `number` | `3` | no |
| <a name="input_os_disk_snapshot_search_string"></a> [os\_disk\_snapshot\_search\_string](#input\_os\_disk\_snapshot\_search\_string) | Set to the name of an instance, in order to use an existing snapshot of it's boot disk for a new node. | `string` | `""` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-ssd"` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | `null` | no |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_snapshot_project"></a> [snapshot\_project](#input\_snapshot\_project) | Project from which snapshots should be used from during provisioning. This should generally be unset and left default | `string` | `""` | no |
| <a name="input_subnetwork_self_link"></a> [subnetwork\_self\_link](#input\_subnetwork\_self\_link) | Self-link of the subnetwork for the instance. | `string` | `null` | no |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | user-provided teardown script to override the bootstrap module | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | The instance availability zone | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ipaddress"></a> [ipaddress](#output\_ipaddress) | n/a |
| <a name="output_name"></a> [name](#output\_name) | n/a |
| <a name="output_project"></a> [project](#output\_project) | n/a |
| <a name="output_zone"></a> [zone](#output\_zone) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
